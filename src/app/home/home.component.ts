import {Component, OnInit, ViewChild} from '@angular/core';

import {Car} from '../models/car';
import {CarService} from '../shared/car.service';
import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({selector: 'my-home', templateUrl: './home.component.html', styleUrls: ['./home.component.scss']})
export class HomeComponent implements OnInit {
  private carList : Array < Car >;
  car : Car = new Car();
  editedCar : Car = new Car();

  @ViewChild('lgModal')newCarModal : ModalDirective;
  @ViewChild('editModal')editCarModal : ModalDirective;

  constructor(private carService : CarService) {
    this
      .carService
      .getCarList()
      .then(x => {

        this.carList = x;
        console.log(this.carList);
      });
  }

  ngOnInit() {
    console.log('Hello Home');
  }

  deleteCar(carIndex) {
    this
      .carList
      .splice(carIndex, 1);
  }

  updateCar(car) {
    this
      .editCarModal
      .show();
    this.editedCar = car;
  }

  addNewCar() {
    console.log(this.car);
    let newCar : Car = new Car();
    newCar.id = 1;
    newCar.model = this.car.model;
    newCar.avatar = this.car.avatar;
    newCar.manufacturer = this.car.manufacturer;
    newCar.createdOn = new Date();
    this
      .carList
      .push(newCar);
    this
      .newCarModal
      .hide();
  }

}
