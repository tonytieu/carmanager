export class Car {
    id : number;
    model : string;
    manufacturer : string;
    avatar : string;
    description : string;
    createdOn : Date;
    images : Array < string >;

}