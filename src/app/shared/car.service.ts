import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Car} from '../models/car';

Injectable()
export class CarService {
    private carList : Array < Car > = [
        {
            id: 1,
            model: "Mazda 3",
            manufacturer: "Maza",
            avatar: '',
            description: 'description',
            createdOn: new Date(),
            images: []
        }, {
            id: 2,
            model: "Fortuner",
            manufacturer: "Toyota",
            avatar: '',
            description: 'description',
            createdOn: new Date(),
            images: []
        }
    ];

    getCarList() : Promise < Array < Car > > {

        return Promise.resolve(this.carList);
    }
}